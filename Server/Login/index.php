<?php

if (!empty($json_str = $_POST['data'])) {
    $user = json_decode($json_str,true);
    if($handle=fopen("../Users.txt","r")){
        $exists = false;
        while($arr=json_decode(fgets($handle),true)){
            if((strcmp($arr['user']['name'],$user['name'])==0)&&
                (strcmp($arr['user']['password'],$user['password'])==0)){
                echo json_encode(array('status'=>true,'id'=>$arr['id'],'message'=>'OK'));
                $exists=true;
                break;
            }
        }
        if(!$exists){
            echo json_encode(array('status'=>false,'id'=>-1,'message'=>'Combination of entered 
                                                                        name and password does not exist'));
        }
    } else{
        echo json_encode(array('status'=>false,'id'=>-1,'message'=>'Can`t open Users.txt'));
    }
} else{
    echo json_encode(array('status'=>false,'id'=>-1,'message'=>'received empty data'));
}
<?php
if (!empty($json_str = $_POST['data'])) {
    $user = json_decode($json_str, true);
    $handle = fopen("../Users.txt", "r");
    $i = 1;
    if ($str = file_get_contents("../Users.txt")) {
        $already_exists = false;
        while ($arr = json_decode(fgets($handle), true)) {
            if (strcmp($arr['user']['name'], $user['name']) == 0) {
                $already_exists = true;
                break;
            }
            $i++;
        }
        fclose($handle);
        if ($already_exists) {
            echo json_encode(array('status' => false, 'id' => -1, 'message' => 'login already exists'));
        } else {
            $data = array('user' => $user, 'id' => $i);
            echo json_encode(array('status' => true, 'id' => $i, 'message' => 'OK'));
            file_put_contents("../Users.txt", json_encode($data), FILE_APPEND);
            file_put_contents("../Users.txt", "\n", FILE_APPEND);
        }
    } else {
        fclose($handle);
        $data = array('user' => $user, 'id' => $i);
        echo json_encode(array('status' => true, 'id' => $i, 'message' => 'OK'));
        file_put_contents("../Users.txt", json_encode($data), FILE_APPEND);
        file_put_contents("../Users.txt", "\n", FILE_APPEND);
    }
} else {
    echo json_encode(array('status' => false, 'id' => -1, 'message' => 'retrieved empty data'));
}
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

CREATE DATABASE IF NOT EXISTS `shop2_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `shop2_db`;

CREATE TABLE `goods` (
  `id_goods` int(11) NOT NULL,
  `name_goods` varchar(255) NOT NULL,
  `cost_goods` decimal(10,0) NOT NULL,
  `quantity_goods` int(11) NOT NULL,
  `image_goods` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `goods` (`id_goods`, `name_goods`, `cost_goods`, `quantity_goods`, `image_goods`) VALUES
(1, 'Paper A4', '1', 1450, 'https://7.allegroimg.com/original/032b36/1a7e92cf46868e68494fbf6b3bb7/50x-FOTO-FOTOGRAFICZNY-PAPIER-GLOSSY-A4-180g-m2'),
(2, 'Eraser', '2', 600, 'https://c.superdelivery.com/ip/n/sa/1200/630/www.superdelivery.com/product_image/69/2/692875_s_1000.jpg'),
(3, 'Pen', '2', 500, 'https://www.tskystationery.com/wp-content/uploads/sites/303/2019/10/4-Double-Pastel-Colored-Crystal-Pen2-400x300.jpg'),
(4, 'Pencil', '2', 600, 'https://www.tskystationery.com/wp-content/uploads/sites/303/2019/10/7-Slogan-Printed-Metal-Cap-Crystal-Pencil-400x300.jpg');


ALTER TABLE `goods`
  ADD PRIMARY KEY (`id_goods`);


ALTER TABLE `goods`
  MODIFY `id_goods` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

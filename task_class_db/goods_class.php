<?php

class Goods
{
    private string $name;
    private int $cost;
    private int $quant;
    private string $image;

    public function __construct(string $name, int $cost, int $quant, $image)
    {
        $this->name = $name;
        $this->cost = $cost;
        $this->quant = $quant;
        $this->image = $image;
    }

    /**
 * @return string
 */public function getName(): string
{
    return $this->name;
}/**
 * @param string $name
 */public function setName(string $name): void
{
    $this->name = $name;
}

    /**
     * @return int
     */
    public function getCost(): int
    {
        return $this->cost;
    }

    /**
     * @param int $cost
     */
    public function setCost(int $cost): void
    {
        $this->cost = $cost;
    }

    /**
     * @return int
     */
    public function getQuant(): int
    {
        return $this->quant;
    }

    /**
     * @param int $quant
     */
    public function setQuant(int $quant): void
    {
        $this->quant = $quant;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage(string $image): void
    {
        $this->image = $image;
    }
}
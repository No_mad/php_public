<?php
/*
$list_of_goods=[['name'=>'Paper A4','cost'=>'0.1$','quant'=>'523512', 'image'=>'img/A4.png'],
                ['name'=>'Blue pen','cost'=>'1.5$','quant'=>'24235',  'image'=>'img/Pen.png'],
                ['name'=>'pencil',  'cost'=>'1$',  'quant'=>'25000',  'image'=>'img/pencil.png'],
                ['name'=>'eraser',  'cost'=>'0.75','quant'=>'15000',  'image'=>'img/eraser.png']];
$list_of_goods_json= json_encode($list_of_goods);
file_put_contents('List.txt',$list_of_goods_json);
*/

require_once 'init.php';

$i = 0;


?>
<!DOCTYPE html>
<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>Shop list</title>
    <div class="container">
        <div class="p-3 mb-2 bg-success text-white">
            <h1>List of goods:</h1>
        </div>
    </div>
</head>
<body class="p-3 mb-2 bg-info text-dark">
<form action="action.php" method="post">
    <div class="container">
        <?php foreach($goods as $arr): ?>
        <div class="p-3 mb-2 bg-secondary text-white">
            <div class="border border-white border-4">
                <div class="row">
                    <div class="col-md-4">
                        <img src="<?php echo $arr->getImage(); ?>" class="rounded" width="200" height="200">
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="fs-3">
                                Name: <?php echo $arr->getName(); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="fs-3">
                                Cost: <?php echo $arr->getCost(); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="fs-3">
                                Quantity: <?php echo $arr->getQuant(); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="fs-3">
                                    Add to cart: <input type="checkbox" name="check[]" value="<?php echo $i; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $i++; endforeach;?>
        <div class="d-grid gap-2">
            <button type="submit" class="btn btn-success">Accept</button>
        </div>
    </div>

</form>
</body>
</html>
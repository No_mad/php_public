<?php
session_start();
require_once 'goods_class.php';

define("DB_NAME", "shop2_db");
define("DB_USER", "admin");
define("DB_PASS", "1234");

$dsn = "mysql:host=localhost;port=3306;dbname=" . DB_NAME . ";charset=utf8";
$pdo = new PDO($dsn, DB_USER, DB_PASS);

$goods = array();
$query = "SELECT * FROM goods";
$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
$stmt = $pdo->query($query);

if($stmt) {
    $result = $stmt->fetchAll();
    foreach ($result as $arr){
        array_push($goods,new Goods($arr['name_goods'],
            $arr['cost_goods'],
            $arr['quantity_goods'],
            $arr['image_goods']));
    }
}
else{
    exit();
}
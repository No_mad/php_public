<?php


function get_users($path)
{
    if ($users_json = file_get_contents($path)) {
        if ($users = json_decode($users_json, true)) {
            echo 'Data acquired successfully<br>';
            return $users;
        } else {
            echo 'Error occurred when trying to decode json<br>';
            return false;
        }
    } else {
        echo 'Error occurred when trying to read from Users.txt<br>';
        return false;
    }
}

function search_user($login, $password, $user_array)
{
    if ((!empty($user_array)) && (!empty($login)) && (!empty($password))) {
        $check = false;
        foreach ($user_array as $arr) {
            if ((strcmp($login, $arr['login']) == 0) && (strcmp($password, $arr['pass']) == 0)) {
                $rez = $arr;
                $check = true;
                break;
            }
        }
        if ($check) {
            echo 'User found<br>';
            return $rez;
        } else {
            echo 'Incorrect login and password combination<br>';
            return false;
        }
    } else {
        echo 'Incorrect parameters<br>';
        return false;
    }
}

$myarr = get_users('Users.txt');
if ($rez1 = search_user('jhon4u', 'qwerty1234', $myarr)) {
    var_dump($rez1);
    echo '<br>';
}
if ($rez2 = search_user('someone', 'pass', $myarr)) {
    var_dump($rez2);
    echo '<br>';
}




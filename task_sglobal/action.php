<?php

require_once 'init.php';

if (!empty($_SESSION['list']) && empty($_POST['check'])) {
    $list = json_decode($_SESSION['list'], true);
} else {
    if (!empty($checkboxes = $_POST['check'])) {
        $list = array();
            foreach ($checkboxes as $arr) {
                if (isset($arr)) {
                    array_push($list, $goods[$arr]);
                }
            }
            $_SESSION['list'] = json_encode($list);
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>Shopping basket</title>
    <div class="container">
        <div class="p-3 mb-2 bg-success text-white">
            <h1>Shopping basket:</h1>
        </div>
    </div>
</head>
<body class="p-3 mb-2 bg-info text-dark">
<form action="index.php" target="_blank">
    <div class="container">
        <?php foreach($list as $arr): ?>
            <div class="p-3 mb-2 bg-secondary text-white">
                <div class="border border-white border-4">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="<?php echo $arr['image']; ?>" class="rounded" width="200" height="200">
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="fs-3">
                                    Name: <?php echo $arr['name']; ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="fs-3">
                                    Cost: <?php echo $arr['cost']; ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="fs-3">
                                    Quantity: <?php echo $arr['quant']; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
            <div class="d-grid gap-2">
                <button class="btn btn-success">Go to main menu</button>
            </div>
    </div>
</form>
</body>
</html>
